import numpy as np
import skimage
import sys
import os

rgb_convert = skimage.color.rgb2hsv


def dummy_project_frame(values, x_pos, y_pos, out_shape, sigma=0, gain=1):
    n_values, n_colors = values.shape
    output = np.zeros(out_shape + (n_colors,), values.dtype)
    output[y_pos.flat[:n_values], x_pos.flat[:n_values], :] = values
    if not sigma:
        return output
    else:
        filtered = gain * skimage.filters.gaussian(output, sigma=sigma, multichannel=True, preserve_range=False)
        return filtered



def dummy_project_video(values, x_pos, y_pos, out_shape, sigma=0, gain=1):
    n_frames, n_colors = values.shape[0], values.shape[-1]
    output = np.zeros( (values.shape[0],) + out_shape + (n_colors,), values.dtype)
    for i in range(n_frames):
        output[i] = dummy_project_frame(values[i], x_pos, y_pos, out_shape, sigma, gain)
    return output


if __name__ == "__main__":
    ifn = sys.argv[1]
    ofn = os.path.splitext(ifn)[0] + '.simulated.npy'
    rgb_data = skimage.io.imread(ifn) #skimage.io.ImageCollection(ifn + '/*.png').concatenate()

    #print(rgb_data.shape) # ( nframes, nu, nn, 3 )
    nframes, nleds, _ = rgb_data.shape
    hsv_data = rgb_convert(rgb_data)
    h, s, v = np.rollaxis(hsv_data, -1)

    skimage.io.imsave("test_images/h_img.png", (h*255).astype('u1'))   # hue + saturation = data value
    skimage.io.imsave("test_images/s_img.png", (s*255).astype('u1'))
    skimage.io.imsave("test_images/v_img.png", (v*255).astype('u1'))   # value = data presest (black vs non-black)

    # determine hue shift (ignore for now)

    padding = 12
    dummy_frame_wh = 512, 500
    pos_sigma = 1

    n_leds = hsv_data.shape[1]
    grid_w = np.ceil(n_leds ** 0.5) # overestimates number but that's ok
    grid_h = grid_w

    x_pos_list = np.linspace(padding, dummy_frame_wh[0] - padding, grid_w).astype('u2')
    y_pos_list = np.linspace(padding, dummy_frame_wh[1] - padding, grid_h).astype('u2')
    x_pos, y_pos = np.meshgrid(x_pos_list, y_pos_list)
    x_pos = np.rint(np.random.normal(x_pos, scale=pos_sigma, size=x_pos.shape)).astype('i2')
    y_pos = np.rint(np.random.normal(y_pos, scale=pos_sigma, size=y_pos.shape)).astype('i2')

    print( 'projecting %d frames' % rgb_data.shape[0] )
    rgb_fake_frames = dummy_project_video(rgb_data, x_pos, y_pos, dummy_frame_wh[::-1], sigma=3, gain=255 * 30)

    print ('simulate frames shape=', rgb_fake_frames.shape)
    print(rgb_fake_frames.min(axis=(0,1,2)), rgb_fake_frames.max( axis=(0,1,2)))

    np.save(ofn, rgb_fake_frames )
