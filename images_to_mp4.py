#!/usr/bin/env python
import numpy as np
import matplotlib
matplotlib.use('qt4agg') # <-- THIS MAKES IT FAST!
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from datetime import datetime
import matplotlib.animation as manimation
import pickle
import time
import sys
import os

class PlotImage(object):
	def __init__(self, fig, gs, **kwargs):
		self.fig = fig
		self.gs = gs
		self.args = kwargs
		self.plot = None
		ax = plt.Subplot(fig, gs)
		ax.axis('off')
		fig.add_subplot(ax)
		self.ax = ax

	def __call__(self, img):
		if not self.plot:
			self.plot = self.ax.imshow(img, interpolation='nearest', **self.args )
		else:
			self.plot.set_data(img)


if __name__ == "__main__":
	ifn = sys.argv[1]
#	 pickle.load(file(ifn, 'rb')) )
	ofn = os.path.splitext(ifn)[0] + '.mp4'

	data = np.load(ifn)
	print('data shape =', data.shape)

	assert data.ndim in (3,4)

	if data.ndim == 3:
		plot_args = {'cmap':'hot', 'clim':(0.0, data.max()) }
	elif data.ndim == 4:
		plot_args = {}

	plt.ion()
	plt.show(block=False)

	pagefig = plt.figure( figsize=(8,8) )
	pagegrid = gridspec.GridSpec( 1, 1, wspace=0.1, hspace=0.1 )
	plot_image = PlotImage( pagefig, pagegrid[0], **plot_args )
	pagegrid.tight_layout( pagefig, rect=[0, 0, 1, 1], h_pad=0.5 )
	metadata = {}#dict(title='SLEDS sweep', artist='Andrea Waite')
	writer = manimation.FFMpegWriter(fps=15, bitrate=500, codec='libx264', metadata=metadata)

	ms = 20
	with writer.saving(pagefig, ofn, ms):
		for i in range(len(data)):
			plot_image(data[i])
			pagefig.canvas.update()
			pagefig.canvas.flush_events()
#	        pagefig.savefig('sweep_sim/'+datetime.now().strftime('%y%m%d-%H%M-') + '%-.2d.png' % i)
			writer.grab_frame()
