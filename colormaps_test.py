from matplotlib import *
import matplotlib.pyplot as pyplot
from numpy import linspace, outer
from pylab import *


rc('text', usetex=False)
a=outer(arange(0,1,0.01),ones(255))
fig= pyplot.figure(figsize=(13,7), dpi=120, facecolor=[1,1,1])
subplots_adjust(top=0.8,bottom=0.05,left=0.01,right=0.99)
maps=[m for m in cm.datad.keys() if not m.endswith("_r")]
maps.sort()
l=len(maps)+1
i=1
for m in maps:
    print(m)
    subplot(1,l,i)
    axis("off")
    imshow(a,aspect='auto',cmap=get_cmap(m),origin="lower")
    title(m,rotation=90,fontsize=9)
    i=i+1
show()
