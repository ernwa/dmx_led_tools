import numpy as np
import matplotlib.pyplot as plt
import skimage

# import images
# convert to hsv
# normalize brightnesses / background subtract (sliding window?)
# smooth/median filter across ~2 frames?
# quantize to symbols
# edge detect
# decode
# error correct
# region label
# threshold to consistency
# integrate non-normalized brightness with labeled regions
# determine centroid
# output address labelling map ( pixel coordinates )

# another code: combine multiple point labels to obtain 3d map using photogrammetry?
# perhaps combine labeled regions, then find centroid on mapped surface?
# what library can do surface centroid finding? maybe do it on flattened surface?
