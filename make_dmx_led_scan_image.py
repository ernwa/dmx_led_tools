import numpy as np
import math
import argparse
from imageio import imsave


# color_codes = {'K':(0,0,0), 'B':(0,0,1), 'G':(0,1,0), 'C':(0,1,1), 'R':(1,0,0), 'M':(1,0,1), 'Y':(1,1,0), 'W':(1,1,1)}
# color_wheel = 'KBGCRMYW'


color_cycle = np.array(((0,0,0), (0,0,1), (0,1,0), (0,1,1),
                        (1,0,0), (1,0,1), (1,1,0), (1,1,1)), 'u1') * 255


def to_base(A, ndigits, base):
    A = A.copy()
    B = np.zeros(A.shape + (ndigits,), 'u1')
    for i in range(ndigits):
        factor = base ** (ndigits-i-1)
        np.divmod(A, factor, B[...,i], A )
    return B


def limit_run_length(A):
    prev_a = A[ ..., 0 ]
    for i in range(1, A.shape[-1]):
        a = A[ ..., i ]
        a[ (a != 0) & (a == prev_a) ] = 7
        prev_a = a
    return A


def generate_color_codes(addresses, universe_digits=4):
    n_addrs = addresses.shape[0]
    code_len = 2 + universe_digits + 3 + 1
    out_addr = np.zeros( addresses.shape[:-1] + (code_len,), 'u1')
    out_addr[ ...,  1    ] = 7
    out_addr[ ...,  2 : -4 ] = to_base(addresses[...,0], universe_digits, 6) + 1
    out_addr[ ..., -4 : -1 ] = to_base(addresses[...,1], 3, 6) + 1

    limit_run_length(out_addr)

    return out_addr


def universe_to_dmx( cols ):
    output = np.zeros( (cols.shape[0], 512), 'u1')
    colors = color_cycle[ cols ].reshape( cols.shape[0], -1 )
    output[ :, :colors.shape[1] ] = colors
    return output


def led_to_addr( lednum ):
    return divmod(led_spacing * n_leds, 170)



LEDS_PER_UNIV = 170


class DMXPatternMaker(object):
    def __init__(self, universe_range, led_spacing=3, n_repeats=3):

        self.led_spacing = int(led_spacing)
        self.n_code_repeats = n_repeats
        self.u_first, self.u_last = universe_range
        self.n_leds = (1 + self.u_last - self.u_first) * LEDS_PER_UNIV
        self.u_code_len = math.ceil( math.log( self.u_last, 6 ) )
        print( '%d digits in code for universe ids up to %d' % (self.u_code_len, self.u_last) )

        u_nums = np.arange(self.u_first, self.u_last + 1)
        n_nums = np.arange(0, LEDS_PER_UNIV)
        self.led_un = np.ascontiguousarray( np.array( np.meshgrid(u_nums, n_nums, indexing='ij'), 'u2' ).reshape(2,-1).T )
        self.led_un_codes = generate_color_codes( self.led_un, self.u_code_len )

        self.un_code_len = self.led_un_codes.shape[-1]


    def make_cohort_un_codes(self, icohort):
        cohort = np.arange(icohort, self.n_leds, self.led_spacing)

        print( 'driving LED cohort %d/%d: %d on ' % (icohort + 1, self.led_spacing, len(cohort)) )
        cohort_u = self.led_un[ cohort, 0 ]
        cohort_n = self.led_un[ cohort, 1 ]

        u_set = np.unique( cohort_u )
        i_invalid_u = u_set.max() + 1

        u_to_iu = np.full( i_invalid_u, i_invalid_u )
        u_to_iu[ u_set ] = np.arange( u_set.shape[0] )

        cohort_un_codes = np.zeros( (u_set.shape[0], LEDS_PER_UNIV, self.un_code_len), 'u1' )
        cohort_un_codes[ u_to_iu[ cohort_u ], cohort_n ] = self.led_un_codes[ cohort, : ]
        return u_set, cohort_un_codes


    def __iter__(self):
        self.u_set = []
        self.irepeat = 0
        self.icohort = 0
        self.isymbol = 0
        return self


    def __next__(self):
        if self.irepeat >= self.n_code_repeats:
            raise StopIteration()

        if self.isymbol == 0:
            self.u_set, self.cohort_un_codes = self.make_cohort_un_codes(self.icohort)

        cohort_symbol_dmx_data = universe_to_dmx( self.cohort_un_codes[ ..., self.isymbol ])
        labelled_data  = { u:cohort_symbol_dmx_data[iu] for iu, u in enumerate( self.u_set )
                    if self.u_first <= u <= self.u_last }

        a, self.isymbol = divmod(self.isymbol + 1, self.un_code_len)
        b, self.icohort = divmod(self.icohort + a, self.led_spacing)
        self.irepeat += b

        return labelled_data





if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Scan sACN-connected LEDs with address codes')

    parser.add_argument('-start', type=int, default=1, help="start universe")
    parser.add_argument('-end', type=int, default=5, help="end universe")


    parser.add_argument('-maxleds', type=int, default=300, help="max number of leds to turn on at once")
    parser.add_argument('-spacing', type=int, default=2, help="min pixel spacing")
    parser.add_argument('-repeats', type=int, default=3, help="divide dmx hz by this rate")
    parser.add_argument('-loops', type=int, default=4, help="times to repeat displaying codes")

    args = parser.parse_args()

    start_universe = args.start
    end_universe = args.end
    assert 0 <= start_universe <= end_universe
    n_lit_leds = args.maxleds
    N_CODE_REPEATS = args.repeats

    n_universes = end_universe - start_universe + 1
    n_leds = n_universes * LEDS_PER_UNIV
    even_led_spacing = int( np.ceil( n_leds / n_lit_leds ) )
    min_led_spacing = args.spacing
    led_spacing = max(min_led_spacing, even_led_spacing)

    output_images = []

    led_data = DMXPatternMaker( (start_universe, end_universe), led_spacing, args.repeats )
    for i, symbol_data in enumerate(led_data):
        u_set = set(symbol_data.keys())
        print('step %d' % i)
        output_image = np.zeros((n_universes, 510), 'u1')
        for u, ud in symbol_data.items():
            output_image[u - start_universe] = ud[:510]
        output_images += [output_image] * args.repeats

    print("* saving  ")
    output_images_joined = np.array(output_images)
    output_images_joined.shape = (-1, n_leds, 3)
    imsave("led_address_drive_%d-%d.png" % (start_universe, end_universe), output_images_joined)
