import numpy as np
import matplotlib.pyplot as plt
import pickle
import os
import sys
from scipy.spatial import cKDTree
from imageio import imsave


def display_points(xypts, extents):
    l,t,r,b = extents
    plt.figure()
    plt.axhline(t)
    plt.axhline(b)
    plt.axvline(l)
    plt.axvline(r)
    plt.plot(xypts[:,0], xypts[:,1], '.k')
    w = r - l
    h = b - t
    x_pad = w * 0.1
    y_pad = h * 0.1
    plt.ylim((t - y_pad, b + y_pad))
    plt.xlim((l - x_pad, r + x_pad))


def extract_dict_fields(d, keyname, fields):
    field_dict = { f:[] for f in fields }
    field_dict[keyname] = []
    for key, subdictlist in d.items():
        assert len(subdictlist) == 1
        subdict = subdictlist[0]
        field_dict[keyname].append(key)
        for f in fields:
            field_dict[f].append(subdict[f])
    return field_dict


found_leds = pickle.load( open('led_info.pkl', 'rb') )

flat_data = extract_dict_fields(found_leds, 'address', ('weighted_centroid', 'max_intensity', 'equivalent_diameter'))
led_xy = np.array(flat_data['weighted_centroid'])
(led_l, led_t), (led_r, led_b) = np.floor(led_xy.min(axis=0)).astype('i4'), np.ceil(led_xy.max(axis=0)).astype('i4')
led_extents = (led_l, led_t, led_r, led_b)
led_w = led_r - led_l
led_h = led_b - led_t

#kdtree = cKDTree(led_xy)

display_points(led_xy, led_extents)

led_xy_i = np.rint(led_xy - (led_l, led_t)).astype('i2')

led_x = np.rint(led_xy[:,0] - led_l).astype('i4')
led_y = np.rint(led_xy[:,1] - led_t).astype('i4')
led_un = np.array(flat_data['address'])

pickle.dump( {'wh':(led_w, led_h), 'xy':led_xy, 'un':led_un}, open('led_info.pkl', 'wb'), -1 )

#( (led_un[:,0] << 8) | led_un[:,1] ).astype('>u4')
out_img = np.zeros((led_y.max() + 1, led_x.max() + 1, 3), 'u1')
out_img[led_y, led_x, 2] = led_un[:,1]
out_img[led_y, led_x, 1] = led_un[:,0] & 0xFF
out_img[led_y, led_x, 0] = led_un[:,0] >> 8

imsave('led_info.png', out_img)
#plt.triplot(led_xy[:,0], led_xy[:,1], triangles.simplices)
# indptr, indices = triangles.vertex_neighbor_vertices
# for k in range(len(led_xy)):
#     ineighbor = indices[indptr[k]:indptr[k+1]]
#     print( '%d: (%d, %d) -> ' % (k, led_xy[k,0], led_xy[k,1]), end='')
#     for i in ineighbor:
#         print( '(%d, %d) ' % tuple(led_xy[i]), end='')
#     print()

plt.figure()
plt.imshow(out_img)
plt.show()
