import numpy as np
import skimage
import imageio
import sys
import pickle
import os
import matplotlib.pyplot as plt

#led_address_image = skimage.io.imread(sys.argv[1]) # RG = universe, B = dmx node

# calculate nearest neighbor
# create gaussian sample window with standard deviation controlled by distance to nearest neighbor

scale = 2
vid_sample_blur_sigma = 2 # pixels

un_dtype = np.dtype([('u', 'u2'), ('n', 'u2')])

led_fn = sys.argv[1]
led_data = pickle.load(open(led_fn, 'rb'))

led_base_fn = os.path.splitext(led_fn)[0]

video_fn = sys.argv[2]
video_base_fn = os.path.splitext(video_fn)[0]

output_fn = video_base_fn + '.' + led_base_fn + '.vid.png'

led_un = led_data['un']
led_img_pixel = (led_un[:,0]-1) * 170 + (led_un[:,1])
#led_order = np.argsort(led_img_pixel)

led_xy = led_data['xy']
led_w, led_h = led_xy.max(axis=0)
print( '%d leds, wh=( %d, %d )' % (len(led_xy), led_w, led_h) )
#print(led_img_pixel)
led_img_line_width = int( np.rint( np.ceil((led_img_pixel.max()+1) / 170.0) * 170 ) )
print( 'line width', led_img_line_width )


video = np.array(imageio.mimread(video_fn))[...,:3]


print( 'video shape:', video.shape )
n_frames, vid_h, vid_w, n_colors = video.shape
led_ar = led_w / led_h
vid_ar = vid_w / vid_h
print( 'aspect ratios: video=%.2f    led=%.2f' % (vid_ar, led_ar))

# always turn on all LEDs, so

if led_ar < vid_ar:
    print('led narrower')
    # led is narrower; scale leds positions to image height
    led_xy_scale = (vid_h-1) / (led_h)
else:
    print('led wider')
    # led is wider; scale leds positions to image width
    led_xy_scale = (vid_w-1) / (led_w)

led_xy = np.rint(led_xy * led_xy_scale).astype('i2')
print( led_xy_scale )
print(led_xy.max(axis=0), (vid_w, vid_h))

output_image = np.zeros((n_frames, led_img_line_width, 3), 'u1')
print( 'output_image.shape', output_image.shape)
for i in range(n_frames):
    video_frame = skimage.filters.gaussian(video[i], sigma=vid_sample_blur_sigma, multichannel=True, preserve_range=True)
    print(i)
    output_image[i,led_img_pixel] = video_frame[led_xy[:,1], led_xy[:,0]]

print('saving output to %s' % output_fn)
imageio.imsave(output_fn, output_image)

plt.imshow(output_image)
plt.show()


# sample image with each sample window, assign to output image position
# write output image
