import sacn
import time
from itertools import chain
import sys
import socket
import argparse


bad_ip_prefixes = {("169","254"), ("255","255"), ("0","0")}
# preferred_ip_prefixes = {'"169"'}
colordict = { 'K':(0,0,0), 'W':(255,255,255),
           'R':(255, 0, 0), 'G':(0, 255, 0), 'B': (0, 0, 255),
           'C':(0, 255, 255), 'M':(255, 0, 255), 'Y':(255, 255, 0) }

K = colordict['K']


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Scan sACN-connected LEDs')
    parser.add_argument('-ip', type=str, default='', help='bind address for multicast')
    parser.add_argument('-su', type=int, default=1, help="start universe")
    parser.add_argument('-eu', type=int, default=5, help="end universe")
    # parser.add_argument('-sc', type=int, default=1, description="start channel")
    # parser.add_argument('-nl', type=int, default=170, description="number of leds")
    parser.add_argument('-ur', type=float, default=30, help="dmx update rate (hz)")
    parser.add_argument('-sr', type=float, default=15, help="led scan rate (hz)")
    parser.add_argument('-colors', type=str, default='WMRYGCB', help="color cycle for universes")

    args = parser.parse_args()

    assert args.eu >= args.su
    assert all( (c in colordict) for c in args.colors )

    if not args.ip:
        print( 'guessing ip address to bind. options found:' )
        host_addresses = socket.gethostbyname_ex(socket.gethostname())[2]
        print( host_addresses )
        for address in host_addresses:
            if tuple(address.split('.')[:2]) not in bad_ip_prefixes:
                args.ip = address
                break

    pixel_dwell_time = 1.0 / args.sr

    sender = sacn.sACNsender(fps=args.ur, bind_address=args.ip)
    sender.start()  # start the sending thread

    try:

        for ucount, universe in enumerate(range( args.su, args.eu + 1 )):
            print( '%3d: ' % universe, end='' )
            sender.activate_output( universe )
            sender[universe].multicast = True

            for led_number in range( 0, 170 ):
                led_color_str = args.colors[ ucount % len( args.colors )]
                led_color_tuple = colordict[led_color_str]
                print( led_color_str, end='')
                sys.stdout.flush()
                led_data = (K * led_number) + led_color_tuple + (K * (170 - led_number - 1))
                sender[universe].dmx_data = led_data
                time.sleep(pixel_dwell_time)

            print()
            sender[universe].dmx_data = K * 170
            time.sleep(pixel_dwell_time)
            sender.deactivate_output( universe )
        print('sweep complete')

    except KeyboardInterrupt:
        print('\n\nstopping sweep')
        sender[universe].dmx_data = K * 170
        time.sleep(pixel_dwell_time)
        sender.deactivate_output( universe )

    sender.stop()  # do not forget to stop the sender
