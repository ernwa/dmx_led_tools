import numpy as np
import skimage
import sys
import os
from collections import defaultdict
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import pickle

rgb_convert = skimage.color.rgb2hsv # skimage.color.rgb2ycbcr #skimage.color.rgb2lab

rgb_color_cycle = np.array(((0,0,1), (0,1,0), (0,1,1),
                            (1,0,0), (1,0,1), (1,1,0), (1,1,1)), 'u1') * 255

hsv_color_cycle = rgb_convert(rgb_color_cycle.reshape(1,-1,3)).reshape(rgb_color_cycle.shape)

n_symbols = len(rgb_color_cycle)


def extract_code( symbols ):
    symbols = np.asarray(symbols, 'u1')
    notrepeat = np.diff(symbols, axis=0, prepend=-1) != 0
    compacted_code = symbols[ notrepeat ]
    packets = bytes(compacted_code).rstrip(b'\x00').split(b'\x00\x07')[1:]   # unique packet split signal
    count = defaultdict(int)

    for p in packets:
        if p:
            count[p] += 1

    unique_symbols = count.keys()
    total_symbols = len(count)
#    print(compacted_code)
#    print( len(unique_symbols), len(packets) )
    if not count:
        return None, 0.0

    error_rate = float(len(unique_symbols)-1) / len(count)
    maxlikelyhood = tuple(unique_symbols)[ np.argmax(tuple( count.values() )) ]  # select highest count code
    decoded = np.frombuffer(maxlikelyhood, 'u1').copy()
    dup_mask = decoded == 7
    decoded[ dup_mask ] = decoded[ np.roll(dup_mask, -1) ]
    return decoded - 1, error_rate


def from_base(A, base):
    A = np.array(A, 'u1')
    n_digits = A.shape[-1]
    B = np.zeros(A.shape[:-1], 'u8')
    for i in range(n_digits):
        factor = int(base ** (n_digits-i-1))
        np.add(B, factor * A[i], out=B, dtype='u8') # FIXME: why is the dtype required?
    return B


def plot_constellation(hsv_values, color_values):
    print('tabulating')

    h,s,v = np.clip( np.rint(np.rollaxis(hsv_frames, -1).reshape(3,-1) * 255).astype('u2'), 0, 255)
    color = np.zeros( (256,256,256), 'u4')
    color[h,s,v] = color_values.flatten()
    uh, us, uv = np.nonzero(color)
    uc = color[uh, us, uv]
    print('plotting')
    fig = plt.figure()

    ax = Axes3D(fig)
    ax.scatter(uh, us, uv, c=uc, marker='.', edgecolors='face')
    ax.set_xlabel('h')
    ax.set_ylabel('s')
    ax.set_zlabel('v')



def segment_frames(hsv_frames):
    n_frames, frame_h, frame_w = hsv_frames.shape[:3] # rgb video
    frame_shape = frame_h, frame_w
    print( n_frames, frame_shape)

    symbol_scores = np.empty( (n_symbols, frame_h, frame_w) )
    symbol_frames = np.empty( (n_frames, frame_h, frame_w), 'u2' )
    symbol_best = np.empty( (n_frames, frame_h, frame_w) )
    symbol_next = np.empty( (n_frames, frame_h, frame_w) )
    same_symbol_frame_regions = np.empty( (n_frames, frame_h, frame_w) )

    for i, f in enumerate(hsv_frames):
        print( '%4d' % i, end='' )
        sys.stdout.flush()
        in_frame_hsv = hsv_frames[i]
        for isymbol, symbol_hsv in enumerate(hsv_color_cycle):
            sh, ss, sv = symbol_hsv
#            hdiff = (in_frame_hsv[...,0] - sh1)**2
            hdiff = np.min(np.array( (in_frame_hsv[...,0] - sh, 1 + in_frame_hsv[...,0] - sh))**2 , axis=0)
            sdiff = (in_frame_hsv[...,1] - ss)**2
#            vdiff = (in_frame_hsv[...,2] - sv)**2
            h_score = np.sum((hdiff, sdiff), axis=0)
            symbol_scores[ isymbol ] = h_score

        symbol_frames[i] = np.argmin( symbol_scores, axis=0 ) + 1
        symbol_scores.sort(axis=0)
        symbol_best[i] = symbol_scores[0]
        symbol_next[i] = symbol_scores[1]
        symbol_frames[i][in_frame_hsv[...,2] < 0.1] = 0

        same_symbol_frame_regions[i] = skimage.filters.sobel( symbol_frames[i] )

    same_symbol_regions = np.max( np.abs(same_symbol_frame_regions), axis = 0 ) == 0
    no_code_pixel_mask = np.all( symbol_frames == 0, axis=0 )
    same_symbol_regions[ no_code_pixel_mask ] = 0

    # plt.figure()
    # plt.imshow(same_symbol_regions)

    region_labels = skimage.measure.label( same_symbol_regions )

    print('\n\n')
    print( 'found %d separate possible codes' % region_labels.max() )
    return region_labels, symbol_frames, symbol_best, symbol_next


def decode_segments(region_labels, symbol_frames, intensity_frame):
    min_region_area = 4
    found_leds = {}
    for region in skimage.measure.regionprops(region_labels, intensity_frame):
        area = region['area']
        if area  >= min_region_area: #, 'Centroid', 'equivalent_diameter']
            cy, cx = region['weighted_centroid']
            equivalent_diameter = region['equivalent_diameter']
            max_intensity = region['max_intensity']
            y, x = region['coords'][0]
            assert region_labels[y,x] == region['label']
            symbol_vals = symbol_frames[:,y,x]
#            print( symbol_vals )
            region_code, region_code_error = extract_code( symbol_vals )
            if region_code is not None:
                decoded_node = int(from_base( region_code[-3:], 6 ))
                decoded_universe = int(from_base( region_code[:-3], 6 ))
                decoded_address = (decoded_universe, decoded_node)
                found_leds.setdefault(decoded_address, []).append({
                    'slice':region['slice'],
                    'image':region['image'],
                    'weighted_centroid':(cy, cx),
                    'equivalent_diameter':equivalent_diameter,
                    'max_intensity':max_intensity,
                    'area':area})

            # print('centroid=[%4.1f, %4.1f], diam=%.1f, symbols=%s, cert=%d%%, dmx_addr=%d.%d' % (
            #         cx, cy,
            #         equivalent_diameter,
            #         str(region_code),
            #         (1-region_code_error) * 100,
            #         decoded_universe,
            #         decoded_node
            #         ))

    return found_leds


# def generate_fake_frame():


if __name__ == "__main__":

#    print( 'testing code extract', extract_code( (0,0,0,7,2,2,2,7,7,2,3,3,4,0,0,0,7,7,7,2,2,7,2,3,3,4) ) )

    ifn = sys.argv[1]
    print('loading frames from %s' % ifn)
    rgb_frames = np.load(ifn) #skimage.io.imread(ifn) #skimage.io.ImageCollection(ifn + '/*.png').concatenate()

    hsv_frames = np.zeros(rgb_frames.shape)

    print('converting to hsv')
    for i,f in enumerate(rgb_frames):
        hsv_frames[i] = rgb_convert(f)


    # **** THRESHOLDING ****

    frame_std = np.std(hsv_frames[:,:,:,2], axis=(1,2)) # adaptive threshold & segment


    # calculate L2 distance from each symbol's canonical value
    # sqrt( (color_hue - pixel_hue)**2 + (color_saturation - pixel_saturation)**2 )

    print('normalizing brightness')
    hsv_frames[...,1:] /= hsv_frames[...,1:].max(axis=(0,1,2))  # normalize saturation & value

    print('detecting symbols in frames')
    region_labels, quantized_frames, best, next = segment_frames(hsv_frames)

    np.save('quantized_frames.npy', quantized_frames )
    np.save('frame_snr.npy', best )

    intensity_frame = np.max( hsv_frames[...,2], axis=0 )


    # plt.figure()
    # plt.title('frame brightness')
    # plt.plot(np.min(best, axis=(1,2)))
    # plt.plot(np.min(next - best, axis=(1,2)))

#    plt.imshow(intensity_frame)
    # plt.plot(frame_std)
    # plt.plot(np.mean(hsv_fake_frames[:,:,:,2], axis=(1,2)))


    # plt.show()
    # sys.exit()


    # plot_constellation(hsv_frames, quantized_frames)
    # plt.title('hsv constellation')
    #plt.show()
    #sys.exit()

    found_leds = decode_segments( region_labels, quantized_frames, intensity_frame )

    # todo: combine regions to get better centroid calculation?

    region_intensity = np.full_like(intensity_frame, 0)
    region_code = np.full_like(intensity_frame, 0)

    # plt.figure()
    # plt.title('distinct code regions (labels)')
    #
    #
    # plt.imshow(region_labels, cmap='rainbow')

    print('found %d valid LEDs' % len(found_leds))
    print()

    unified_codes = [ ((u-1) * 170 + n) for u, n in found_leds.keys() ]
    min_code = np.min(unified_codes)
    max_code = np.max(unified_codes)
    hue_indices = unified_codes - min_code
    code_hues = np.linspace( 0, 1, (max_code - min_code) + 1 )
    known_code_hues = { un: code_hues[chi] for (un, chi) in zip(found_leds.keys(), hue_indices) }


    centroid_xy = []
    for addr,led_regions in found_leds.items():

        print( len(led_regions), end='' )

        for led in led_regions:
            cy, cx = led['weighted_centroid']
            centroid_xy.append((cx,cy))

            region_slice = led['slice']
            region_image = led['image']

            print('[%4.1f, %4.1f]: d=%.1f, b=%.1f, dmx_addr=%s' % (
                    cx, cy,
                    led['equivalent_diameter'],
                    led['max_intensity'],
                    str(addr)
                    ))

            region_intensity[ region_slice ][ region_image ] = intensity_frame[ region_slice ][ region_image ]
            region_code[ region_slice ][ region_image ] = known_code_hues[ addr ]


    hsv_code_image = np.rollaxis(
                        np.array((
                            region_code,
                            np.ones_like(region_code),
                            region_intensity / region_intensity.max()
                                                                        )), 0, 3)   # change (3, h, w) to (h, w, 3)

    #print( hsv_code_image.shape )
    #print( hsv_code_image.max(axis=(0,1)))
    plt.figure()
    plt.title('LEDs found in image')
    plt.imshow( (skimage.color.hsv2rgb(hsv_code_image) * 255).astype('u1') )

    for cx,cy in centroid_xy:
        plt.plot(cx, cy, 'k.', markersize=1)

    # plt.figure()
    # plt.title('region intensity')
    # plt.imshow(region_intensity)


    print('saving led locations')
    pickle.dump( found_leds, open('led_info.pkl', 'wb'), -1)
    # imshape = h.shape
    #
    #
    #
    # plt.figure()
    # plt.title('v')
    # plt.imshow(v)
    #
    #
    # plt.figure()
    # plt.title('h')
    # plt.imshow(h)
    #
    # plt.figure()
    # plt.hist(h.flat, bins=255)
    #
    # plt.figure()
    # plt.title('s')
    # plt.imshow(s)


    plt.show()
