import sacn
import socket
import threading
import time
from sys import stdout

import types
import logging
#logging.basicConfig(level=logging.INFO)


def add_sync_callback(send_thread, callback):
    send_thread_class = send_thread.__class__

    def send_out_patch(self, *args, **kwargs):
        try:
            send_thread_class.send_out(self, *args, **kwargs)
            if self.enabled_flag:
                callback(self, *args, **kwargs)
        except (StopIteration, KeyboardInterrupt):
            self.enabled_flag = False
            for output in self._outputs.values():
                output.dmx_data = (0,)
                self.send_out(output)

    send_thread.send_out = send_out_patch.__get__(send_thread, send_thread.__class__)


class SendSync(object):
    def __init__(self, sender, universe_sent_callback=None, all_sent_callback=None):
        self.sender = sender
        self.universe_sent_callback = universe_sent_callback
        self.all_sent_callback = all_sent_callback
        self.last_time = time.time()
        self.to_send = self.active_universes

    @property
    def active_universes(self):
        return set(self.sender._outputs)

    @property
    def output_universes(self):
        return { o:u for u,o in self.sender._outputs.items() }


    def on_send(self, send_thread, output):
        u_num = self.output_universes[output]

        if self.universe_sent_callback:
            self.universe_sent_callback( u_num, output )

        self.to_send.remove( u_num )

        if not self.to_send:
            self.to_send = self.active_universes
            if self.all_sent_callback:
                self.all_sent_callback()


K = (0, 0, 0)


if __name__ == "__main__":

    STARTU = 1
    NU = 3
    UNIVERSES = range(STARTU, STARTU + NU)

    bind_ip = ''
#    bind_ip = '169.254.80.143'
    sender = sacn.sACNsender(fps=3.0, bind_address=bind_ip, universeDiscovery=False)

    for u in UNIVERSES:
        sender.activate_output( u )
        sender[ u ].multicast = True

    last_time = time.time()


    def print_universe(u, output):
        stdout.write('%d ' % u)


    def print_update():
        global last_time
        now = time.time()
        stdout.write('outputs sent in %.1f ms\n' % ((now - last_time) * 1000 ))
        stdout.flush()
        last_time = now
        for u in UNIVERSES:
            sender[ u ].dmx_data = K * 170


    sync = SendSync(UNIVERSES, universe_sent_callback= print_universe, all_sent_callback=print_update)

    sender.start()

    add_sync_callback( sender._output_thread, sync.on_send )

    for u in UNIVERSES:
        sender[ u ].dmx_data = K * 170

    time.sleep( 5 )

    sender.stop()  # do not forget to stop the sender
