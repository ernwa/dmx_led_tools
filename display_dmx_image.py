#dmx display images

import numpy as np
import math
import argparse
import sacn
import sacn_sync
import imageio
import time
from sys import stdout
from itertools import count

def DisplayLine(sender, input_img, start_universe=1, loops=1, repeats=1):
    loop_list = range(loops) if (loops > 0) else count(0)
    for loop in loop_list:
        for frame in input_img:
            for _ in range(repeats):
                for iu, line in enumerate(frame):
                    u = iu + start_universe
                    sender[u].dmx_data = tuple(line)
                yield
    print('done')

def print_universe(u, output):
    stdout.write('%d ' % u)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Scan sACN-connected LEDs with address codes')
    parser.add_argument('input_fn', type=str, help='input filename (png or gif)')
    parser.add_argument('-bind_ip', type=str, default='', help='bind address for multicast')
    parser.add_argument('-start_ip', type=str, default='', help='ip address of first universe, for unicast'

    parser.add_argument('-start', type=int, default=1, help="start universe for image display")
    parser.add_argument('-fps', type=float, default=30, help="dmx update rate (hz)")
    parser.add_argument('-slow', type=int, default=1, help="times to repeat")

    parser.add_argument('-loops', type=int, default=0, help="loop image times (0 is continuous)")

    args = parser.parse_args()

    input_img = imageio.imread(args.input_fn)
    n_frames = input_img.shape[0]
    input_img.shape = n_frames, -1, 510
    start_universe = args.start
    n_universes = input_img.shape[1]
    end_universe = start_universe + n_universes - 1
    universes = range(start_universe, start_universe + n_universes)

    sender = sacn.sACNsender(fps=args.fps, bind_address=args.bind_ip, universeDiscovery=False)

    for u in universes:
        sender.activate_output( u )
        sender[ u ].multicast = True


    displayer = DisplayLine(sender, input_img, start_universe, args.loops, args.slow)


    # def print_universe(u, output):
    #     stdout.write('%d ' % u)
    #
    # last_time = time.time()
    # def print_update():
    #     global last_time
    #     now = time.time()
    #     stdout.write('outputs sent in %.1f ms\n' % ((now - last_time) * 1000 ))
    #     stdout.flush()
    #     last_time = now
    #     next(displayer)
    #
    #
    sender.start()  # start the sending thread
    sync = sacn_sync.SendSync( sender, all_sent_callback=displayer.__next__ )
    sacn_sync.add_sync_callback( sender._output_thread, sync.on_send )

    loop_name = ('%d' % args.loops) if args.loops else 'continuous'
    print('displaying %s: %s loops of %d frames at %.1f fps on DMX universes %d-%d' %
            (args.input_fn, loop_name, n_frames, args.fps, start_universe, end_universe) )
    try:
        while sender._output_thread.enabled_flag and sender._output_thread.is_alive():
            stdout.write('.')
            stdout.flush()
            time.sleep(0.5)

    except (KeyboardInterrupt, StopIteration):
        print('\n\nstopping sweep')
        sender._output_thread.enabled_flag = False
        time.sleep(0.5)

    sender.stop()  # do not forget to stop the sender
